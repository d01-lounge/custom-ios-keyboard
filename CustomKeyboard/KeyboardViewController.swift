//
//  KeyboardViewController.swift
//  CustomKeyboard
//
//  Created by Michiel on 20/11/14.
//  Copyright (c) 2014 Michiel. All rights reserved.
//

import UIKit

class KeyboardViewController: UIInputViewController {

    @IBOutlet var nextKeyboardButton: UIButton!

    override func updateViewConstraints() {
        super.updateViewConstraints()
    
        // Add custom view sizing constraints here
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let aubergineButton = createButtonWithTitle("🍆", actionName: "didTapButton:", text: "I'm a fucking AUBERGINE")
        self.view.addSubview(aubergineButton)
        
        let chestnutButton = createButtonWithTitle("🌰", actionName: "didTapButton:", text: "Chestnuts are gay")
        self.view.addSubview(chestnutButton)
        
        let deleteButton = createButtonWithTitle("🚫", actionName: "deleteCharacter:", text: "Delete")
        self.view.addSubview(deleteButton)
    
        // Perform custom UI setup here
        self.nextKeyboardButton = UIButton.buttonWithType(.System) as UIButton
    
        self.nextKeyboardButton.setTitle(NSLocalizedString("🌐", comment: "Title for 'Next Keyboard' button"), forState: .Normal)
        self.nextKeyboardButton.sizeToFit()
        self.nextKeyboardButton.setTranslatesAutoresizingMaskIntoConstraints(false)
    
        self.nextKeyboardButton.addTarget(self, action: "advanceToNextInputMode", forControlEvents: .TouchUpInside)
        
        self.view.addSubview(self.nextKeyboardButton)
        
        var nextButtonLeftSideConstraint = NSLayoutConstraint(item: chestnutButton, attribute: .Left, relatedBy: .Equal, toItem: self.view, attribute: .Left, multiplier: 1.0, constant: 30.0)
        
        var deleteButtonLeftSideConstraint = NSLayoutConstraint(item: deleteButton, attribute: .Left, relatedBy: .Equal, toItem: self.view, attribute: .Left, multiplier: 1.0, constant: 60.0)
        
        var nextKeyboardButtonLeftSideConstraint = NSLayoutConstraint(item: self.nextKeyboardButton, attribute: .Left, relatedBy: .Equal, toItem: self.view, attribute: .Left, multiplier: 1.0, constant: 0.0)
        var nextKeyboardButtonBottomConstraint = NSLayoutConstraint(item: self.nextKeyboardButton, attribute: .Bottom, relatedBy: .Equal, toItem: self.view, attribute: .Bottom, multiplier: 1.0, constant: 0.0)
        self.view.addConstraints([nextKeyboardButtonLeftSideConstraint, nextKeyboardButtonBottomConstraint, nextButtonLeftSideConstraint, deleteButtonLeftSideConstraint])
    }
    
    func createButtonWithTitle(title: String, actionName: Selector, text: String) -> UIButton {
        let button = UIButton.buttonWithType(.Custom) as UIButton
        
        button.frame = CGRectMake(0, 0, 30, 90)
        button.setTitle(title, forState: .Normal)
        button.setTitle(text, forState: .Selected)
        button.sizeToFit()
        button.titleLabel?.font = UIFont.systemFontOfSize(30)
        button.setTranslatesAutoresizingMaskIntoConstraints(false)
        button.setTitleColor(UIColor.darkGrayColor(), forState: .Normal)
        button.addTarget(self, action: actionName, forControlEvents: .TouchUpInside)
        
        //let image = UIImage(named: "calendar.png") as UIImage?
        //button.setBackgroundImage(image, forState: .Normal)
        
        return button
    }
    
    @IBAction func handleGesture(sender: AnyObject) {
        if sender.state == UIGestureRecognizerState.Began
        {
            let alertController = UIAlertController(title: nil, message:
                "Long-Press Gesture Detected", preferredStyle: UIAlertControllerStyle.Alert)
            alertController.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default,handler: nil))
            
            self.presentViewController(alertController, animated: true, completion: nil)
        }
    }
    
    func didTapButton(sender: AnyObject?) {
        let button = sender as UIButton
        
        let title = button.titleForState(.Selected)
        
        let description = button.description
        
        var proxy = textDocumentProxy as UITextDocumentProxy
        
        proxy.insertText(title!)
        
        var minimumPressDuration: CFTimeInterval = 0
        
        // Copy image
        //let image = UIImage(named: "myimage.png")
        //UIPasteboard.generalPasteboard().image = image;
        
        // Copy text
        UIPasteboard.generalPasteboard().string = "copy test"
    }
    
    func copyImage(sender: AnyObject?) {
        let image = UIImage(named: "myimage.png")
        UIPasteboard.generalPasteboard().image = image;
    }
    
    func deleteCharacter(sender: AnyObject?) {
        var proxy = textDocumentProxy as UITextDocumentProxy
        proxy.deleteBackward()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated
    }

    override func textWillChange(textInput: UITextInput) {
        // The app is about to change the document's contents. Perform any preparation here.
    }

    override func textDidChange(textInput: UITextInput) {
        // The app has just changed the document's contents, the document context has been updated.
    
        var textColor: UIColor
        var proxy = self.textDocumentProxy as UITextDocumentProxy
        if proxy.keyboardAppearance == UIKeyboardAppearance.Dark {
            textColor = UIColor.whiteColor()
        } else {
            textColor = UIColor.blackColor()
        }
        self.nextKeyboardButton.setTitleColor(textColor, forState: .Normal)
    }

}
